#include <stdio.h>

int main(void) {
	int z, n = 1, a = 2, b = 3;
#if x == 1
	if(n > 0) {
		if(a > b)
			z = a;
		else
			z = b;
	}
#else
	if(n > 0) {
		if(a > b)
			z = a;
	}
	else
		z = b;
#endif

	printf("%d \n", z);
	return 0;
}
