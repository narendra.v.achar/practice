#include <stdio.h>

void bit_count(unsigned x) {
	int b;
	
	for(b = 0; x != 0; x >>= 1) {
		if(x & 01)
			b++;
	}
	
	printf("Count: %d \n", b);
}

int main(void) {
	int n = 5;
	
	bit_count(n);

	return 0;
}
