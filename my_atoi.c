#include <stdio.h>

int my_atoi(char str[]) {
	int num = 0;
	int i;

	for(i = 0; str[i] >= '0' && str[i] <= '9'; i++) {
		num  = 10 * num + (str[i] - '0');
	}
	
	return num;
}

int main(void) {
	char str[] = "12345";

	int num = my_atoi(str);
	
	printf("%d \n", num);

	return 0;
}
