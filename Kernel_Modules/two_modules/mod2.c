#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include "mod.h"

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Narendra V");
MODULE_DESCRIPTION("Module inter dependency: Module 2");

int fun3(void)
{
        int (*f1)(void);
 
	printk("%s: function 3 called...", __func__);

        f1 = fun1;
        (*f1)();

        return 0;
}

int fun4(void)
{
	int (*f2)(void);

        printk("%s: function 4 called...", __func__);
	
	f2 = fun2;
	(*f2)();        
	
	return 0;
}

static int __init mod2_start(void)
{
	printk("%s: Module Loaded...", __func__);
	
	fun3();
	fun4();
	
	return 0;
}

static void __exit mod2_exit(void)
{
	printk("%s: Module Cleanup...", __func__);
}

module_init(mod2_start);
module_exit(mod2_exit);

EXPORT_SYMBOL(fun3);
EXPORT_SYMBOL(fun4);
