#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include "mod.h"

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Narendra V");
MODULE_DESCRIPTION("Module inter dependency: Module 1");

int fun1(void)
{
	//int (*f4)(void);

	printk("%s: function 1 called...", __func__);
	
	//f4 = fun4;
	//(*f4)();	

	return 0;
}

int fun2(void)
{
	//int (*f4)(void);

	printk("%s: function 2 called...", __func__);
	
	//f4 = fun4;
	//(*f4)();

	return 0;
}

static int __init mod1_start(void)
{
	printk("%s: Module Loaded...", __func__);

	return 0;
}

static void __exit mod1_exit(void)
{
	printk("%s: Module Cleanup...", __func__);
}

module_init(mod1_start);
module_exit(mod1_exit);

EXPORT_SYMBOL(fun1);
EXPORT_SYMBOL(fun2);
