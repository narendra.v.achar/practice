#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Narendra V");

static int value = 10;
module_param(value, int, S_IWUSR);

static int __init start(void)
{
	printk("%s: Entered value is: %d\n", __func__, value);
	
	return 0;
}

static void __exit clean_up(void)
{
	printk("Exiting module\n");
}

module_init(start);
module_exit(clean_up);
