#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include "../mod.h"

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Narendra V");
MODULE_DESCRIPTION("Module inter dependency: Module 2");

extern int fun1(void);

static int __init mod2_start(void)
{
	printk("%s: Module Loaded...", __func__);
	
	fun1();
	
	return 0;
}

static void __exit mod2_exit(void)
{
	printk("%s: Module Cleanup...", __func__);
}

module_init(mod2_start);
module_exit(mod2_exit);
