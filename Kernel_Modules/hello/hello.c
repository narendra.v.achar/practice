#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>

MODULE_LICENSE("GPL");

MODULE_AUTHOR("Narendra V");

MODULE_DESCRIPTION("Hello World LKM!!!");

MODULE_VERSION("v1.0");

static int __init hello_start(void)
{
	printk("%s: Hello World\n", __func__);
	return 0;
}

static void __exit hello_end(void)
{
	printk("%s: Exiting module\n", __func__);
}

module_init(hello_start);
module_exit(hello_end);
