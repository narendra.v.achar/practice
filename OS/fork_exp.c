#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

int main(void)
{
	pid_t pid, cid;

	pid = fork();

	if (pid == 0)
		printf("Child PID: %d\n", getpid());
		//printf("Child Process\n");
	else if (pid < 0) {
		fprintf(stderr, "Fork failed!!!\n");
	}
	else {
		cid = wait(NULL);
		printf("Parent PID: %d\n", getpid());
		//printf("Child PID: %d\n", cid);
	}

	return 0;
}
