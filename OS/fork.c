#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>

int main(void)
{
	pid_t pid;

	pid = fork();

	if (pid == 0) {
		//execlp("/bin/ls", "ls", NULL);
		while (1);
	}
	else if (pid < 0) {
		fprintf(stderr, "Fork failed!!!\n");
		return -1;
	}
	else {
		wait(NULL);
		printf("child complete!!!\n");
	}

	return 0;
}
