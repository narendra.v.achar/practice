#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

int main(void)
{
	int fd;

	char *myfifo = "tmp/myfifo";

	mkfifo(myfifo, 0666);

	char arr1[100], arr2[100];

	while (1) {
		
		// Write

		fd = open(myfifo, O_WRONLY);

		fgets(arr1, 100, stdin);

		write(fd, arr1, strlen(arr1)+1);
		
		close(fd);

		// Read

		fd = open(myfifo, O_RDONLY);

		read(fd, arr2, strlen(arr2));

		printf("%s \n", arr2);

		close(fd);
	}

	return 0;
}
