#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>

int main(void)
{
        int fd1;

        char *myfifo = "tmp/myfifo";

        //mkfifo(myfifo, 0666);

        char str1[100], str2[100];

        while (1) {

		//Read

 		fd1 = open(myfifo, O_RDONLY);

                read(fd1, str2, strlen(str2));

                printf("%s \n", str2);

                close(fd1);

		// Write

 		fd1 = open(myfifo, O_WRONLY);

                fgets(str1, 100, stdin);

                write(fd1, str1, strlen(str1)+1);
                
		close(fd1);
      }

        return 0;
}

