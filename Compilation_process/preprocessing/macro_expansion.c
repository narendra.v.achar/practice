#include <stdio.h>

#define ADD(a, b) (a + b)

#define MAX(a, b) (a > b ? a : b)

#define PI 3.14

int main(void) {
	int a = 10, b = 2;
	printf("%f \n", PI);
	printf("%d \n", ADD(a, b));
	printf("%d \n", MAX(a, b));
	
	return 0;
}
