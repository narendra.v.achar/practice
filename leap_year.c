#include <stdio.h>
#include <stdlib.h>

int check_leap_year(int* year) {
	return ((*year % 4 == 0 && *year % 100 != 0) || *year % 400 == 0) ? 1 : 0;
}

int main(int argc, char* argv[]) {
//	int val = 2020;
	int val = atoi(argv[1]);
	int* year = &val;
	
	if(check_leap_year(year))
		printf("%d is a leap year \n", *year);
	else
		printf("%d is not a leap year \n", *year);
	
	return 0;
}
