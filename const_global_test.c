#include <stdio.h>

volatile const int x = 10;

int main(void)
{
	int *p;

	p = &x;

	*p = 1000;

	printf("%d \n", *p);
	
	return 0;
}
