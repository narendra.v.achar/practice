void strcopy(char *dbuf, char *sbuf)
{
	while (*sbuf != '\0')
		*dbuf++ = *sbuf++;

	*dbuf++ = '\0';
}
