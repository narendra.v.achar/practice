#include <stdio.h>
#include <string.h>
#include <stdlib.h>
//#include "../include/header.h"

#define SIZE 50

int main(void)
{
	int ch;
	char sbuf[SIZE];
	char dbuf[SIZE];

	do {
		printf("--------------------------------------\n");
		printf("String Programs\n");
		printf("--------------------------------------\n");

		printf("1. Given a string “Global Edge” in a character buffer sbuf, write a function void strcpy (char *dbuf, char *sbuf) to copy the string from sbuf to character buffer dbuf.\n");

		printf("\nENTER 0 TO EXIT\n\n");
		printf("Enter your choice: ");
		scanf("%d", &ch);
		printf("\n");

		switch (ch) {
			case 0 : exit(0);

			case 1 : printf("Enter the string: ");

				 //scanf("%s", sbuf);
				 fgets(sbuf, SIZE, stdin);
				
				 if (strlen(sbuf) > 0)
				 	strcopy(dbuf, sbuf);

				 printf("\nCopied string: %s \n\n", dbuf);

				 break;

			default : printf("Invalid choice!!!\n\n");
		}

	} while(1);

	return 0;
}
