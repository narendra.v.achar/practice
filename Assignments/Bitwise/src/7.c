int count_bit_set(int num)
{
	int count = 0;

	for (int i = 0; i <= (sizeof(num)*8) - 1; i++) {
		if ((num >> i) & 1U)
			count++;
	}
	return count;
}

int count_bit_clear(int num)
{
	int count = 0;

	for (int i = 0; i <= (sizeof(num)*8) - 1; i++) {
		if (((num >> i) & 1U) == 0)
			count++;
	}
	return count;
}
