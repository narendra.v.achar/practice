#include <stdio.h>
#include <stdarg.h>

int fun(int c, ...)
{
	int max, a;
	va_list ap;

	va_start(ap, c);

	for (int i = 1; i <= c; i++)
		if((a = va_arg(ap, int)) > max)
			max = a;

	va_end(ap);

	return max;
}

int main(void)
{
	printf("%d \n", fun(6, 1, 3, 8, 3, 9, 2));

	return 0;
}
