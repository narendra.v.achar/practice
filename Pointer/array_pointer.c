#include <stdio.h>

int main(void)
{
	int a[] = {10, 20, 30, 40, 50, 60};
	int *p = a;

	for(int i = 0; i < sizeof(a)/sizeof(a[0]); i++)
		printf("%d ", *(p + i));

	return 0;
}
