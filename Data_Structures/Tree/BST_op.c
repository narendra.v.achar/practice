#include <stdio.h>
#include <stdlib.h>
#include <time.h>

typedef struct Node {
	int data;
	struct Node *left;
	struct Node *right;
} Node;

Node *createNode(int x)
{
	Node *newnode = (Node *)malloc(sizeof(Node));

	newnode->data = x;
	newnode->left = NULL;
	newnode->right = NULL;

	return newnode;
}

Node *insert(Node *root, int x)
{
	Node *newnode = createNode(x);

	if (root == NULL)
	{
		root = newnode;
		return root;
	}

	if (x <= root->data)
		root->left = insert(root->left, x);
	else if (x > root->data)
		root->right = insert(root->right, x);

	return root;
}

Node *minValNode(Node *root)
{
	Node *cur = root;

	while (cur && cur->left != NULL)
		cur = cur->left;

	return cur;
}

Node *delete(Node *root, int x)
{
	if (root == NULL)
		return root;

	if (x < root->data)
		root->left = delete(root->left, x);
	else if (x > root->data)
		root->right = delete(root->right, x);
	else {
		if (root->left == NULL) {
			Node *temp = root->right;
			free(root);
			return temp;
		}

		if (root->right == NULL) {
			Node *temp = root->left;
			free(root);
			return temp;
		}

		Node *temp = minValNode(root->right);

		root->data = temp->data;

		root->right = delete(root->right, temp->data);
	}

	return root;
}

void inorder(Node *root)
{
	if (root == NULL)
		return;

	inorder(root->left);
	printf("%d ", root->data);
	inorder(root->right);
}

void preorder(Node *root)
{
	if (root == NULL)
		return;

	printf("%d ", root->data);
	preorder(root->left);
	preorder(root->right);
}

void postorder(Node *root)
{
	if (root == NULL)
		return;

	postorder(root->left);
	postorder(root->right);
	printf("%d ", root->data);
}

void level(Node *root, int i)
{
	if (root == NULL)
		return;
	if (i == 1)
		printf("%d ", root->data);
	else if (i > 1) {
		level(root->left, i - 1);
		level(root->right, i - 1);
	}
}

int height(Node *root)
{
	if (root == NULL)
		return 0;

	int lheight = height(root->left);
	int rheight = height(root->right);

	if (lheight <= rheight)
		return rheight + 1;
	else 
		return lheight + 1;
}

void levelorder(Node *root)
{
	int h = height(root);

	for (int i = 0; i < h; i++)
		level(root, i);
}

void display(Node *root)
{
	printf("Preorder: ");
        preorder(root); printf("\n");

        printf("Inorder: ");
        inorder(root); printf("\n");

        printf("Postorder: ");
        postorder(root); printf("\n");

	printf("Level Order: ");
	levelorder(root); printf("\n");
}

int main(void)
{
	Node *root = NULL;

	int x;
	//srand(time(0));

/*	for (int i = 0; i < 15; i++) {
		x = rand() % 100;

		printf("%d ", x);
		root = insert(root, x);
	}
*/

	root = insert(root, 50);
	root = insert(root, 100);
	root = insert(root, 99);
	root = insert(root, 30);
	root = insert(root, 35);
	root = insert(root, 15);
	root = insert(root, 14);
	root = insert(root, 101);
	root = insert(root, 50);

	printf("\n");

	display(root);
	printf("\n");

	root = delete(root, 50);

	display(root);

	return 0;	
}
