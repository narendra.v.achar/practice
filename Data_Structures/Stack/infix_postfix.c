#include <stdio.h>

#define MAX 100

char s[MAX];
int top = -1;

int isempty()
{
	return top == -1;
}

int peek()
{
	return s[top];
}

void push(char ele)
{
	if (top == MAX - 1)
		return;

	s[++top] = ele;
}

char pop()
{
	if (top == -1)
		return -1;

	return s[top--];
}

int isOperand(char ch)
{
	return (ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z');
}

int precedence(char ch)
{
	switch (ch) {
		case '+' :
		case '-' :
			return 1;
		case '*' :
		case '/' : 
			return 2;
		case '^' :
			return 3;
	}
	return -1;
}

int convertToPostfix(char *exp)
{
	int i, j = -1;

	for (int i = 0; exp[i]; i++) {
		if (isOperand(exp[i]))
			exp[++j] = exp[i];
		
		else if (exp[i] == '(')
			push(exp[i]);
		
		else if (exp[i] == ')') {
			while (!isempty() && peek() != '(')
				exp[++j] = pop();
			if (!isempty() && peek() != '(')
				return -1;
			else
				pop();
		}
		else {
			while (!isempty() && precedence(exp[i]) <= precedence(peek()))
				exp[++j] = pop();
			push(exp[i]);
		}
	}

	while (!isempty())
		exp[++j] = pop();

	exp[++j] = '\0';

	printf("%s \n", exp);
}

int main(void)
{
	char exp[] = "a+b*c+d";

	convertToPostfix(exp);

	return 0;
}
