#include <stdio.h>
#include "random.h"

int main(void)
{
	int n = 10, a[n];

	for (int i = 0; i < n; i++) {
		a[i] = gen_ran(100);
		printf("%d ", a[i]);
	}
	printf("\n");

	for (int i = 0; i < n-1; i++) {
		for (int j = i; j < n; j++) {
			if (a[i] > a[j]) {
				int temp = a[i];
				a[i] = a[j];
				a[j] = temp;
			}
		}
	}

	for (int i = 0; i < n; i++)
		printf("%d ", a[i]);

	printf("\n");

	return 0;
}
