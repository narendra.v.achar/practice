#include <stdlib.h>
#include <time.h>

int gen_ran(int n)
{
	return rand() % n;
}

int randomize(int n)
{
	srand(time(0));

	return rand() % n;
}
