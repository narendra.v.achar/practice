#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define max 11

void display();

int q[max];
int front = -1, rear = -1;

void enqueue(int ele)
{
	if (rear == max - 1) {
		printf("Queue is full!!! \n");
		return;
	}

	if (front == -1 && rear == -1) {
		++rear;
		++front;
	} else {
		++rear;
	}

	q[rear] = ele;

	display();
}

int dequeue()
{
	int ele;
	if (front == -1) {
		printf("Queue is empty!!! \n");
		return -1;
	}
	if (front == rear) {
		ele = q[front];
		front = rear = -1;
	}
	else
		ele = q[front++];
	
	display();
	return ele;
}

void display()
{
	for (int i = front; i <= rear; i++)
		printf("%d ", q[i]);

	printf("\n");
}

int main(void)
{
	int x;
	srand(time(0));

	for (int i = 0; i < 10; i++) {
		x = rand() % 100;
		enqueue(x);
	}

	dequeue();
	dequeue();

	return 0;
}
