#include <stdio.h>

int add(int a, int b)
{
	return a + b;
}

int main(void)
{
	int a = 10, b = 20;

	int sum = add(a, b);
	printf("%d \n", sum);

	return 0;
}
