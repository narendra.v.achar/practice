A
A
C
C
a
A
B
B
B
B
a
C
#include <stdio.h>

typedef struct Tag {
	int a;
	char b;
	char c;
	char d;
} Tag;

int main(void)
{
	Tag t;
	printf("%ld\n", sizeof(t.a));
	printf("%ld\n", sizeof(t.b));
	printf("%ld\n", sizeof(t.c));
	printf("%ld\n", sizeof(t.d));
	
	printf("%ld\n", sizeof(t));
	return 0;
}
