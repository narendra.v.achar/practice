#include <stdio.h>

struct point {
	int x;
	int y;
};

struct point fun(int x, int y)
{
	struct point temp;
	temp.x = x;
	temp.y = y;

	return temp;
} 

int main(void)
{
	int x = 10, y = 20;

	struct point p = fun(x, y);
	
	printf("%d\n%d\n", p.x, p.y); 	

	return 0;
}
